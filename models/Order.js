const mongoose = require("mongoose");

const Product = require("../models/Product");

const Users = require("../models/Users");

const orderSchema = new mongoose.Schema({


	totalAmount:{
		type:Number,
		required:[true,"total amount is required."] 
	},

	productName:{
		type: String,
		required:[true, "Product Name is required!"]
	},
	
	quantity:{
		type:Number,
			default: 1
	},
	purchaseDate:{
		type: Date,
			default: new Date()
	}

})



module.exports = mongoose.model("Order",orderSchema);