const express = require("express");

const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const{verify,verifyAdmin} = auth;

router.post("/",verify,verifyAdmin,productControllers.addProduct);

router.get("/active",productControllers.activeProducts);

router.get("/singleProduct/:productId",productControllers.singleProduct);

router.put("/updateProduct/:productId",verify,verifyAdmin,productControllers.updateProduct);

router.delete("/archiveProduct/:productId",verify,verifyAdmin,productControllers.archiveProducts);


module.exports = router;