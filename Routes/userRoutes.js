const express = require("express");
const router = express.Router();


const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify,verifyAdmin} = auth;

//regist route
router.post("/",userControllers.registerUser);
//login 
router.post("/login",userControllers.userLogin);
//details
router.get("/details",verify,userControllers.getUserDetails);
//setting admin
router.put("/setAdmin/:userId",verify,verifyAdmin,userControllers.setAsAdmin);



module.exports = router;

