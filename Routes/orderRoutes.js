const express = require("express");

const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");

const{verify,verifyAdmin} = auth;


router.post('/',verify,orderControllers.createOrders);

router.get("/recentOrder",verify,orderControllers.retrieveUserOrder);

router.get("/allOrders",verify,verifyAdmin,orderControllers.retrieveAllOrder);


module.exports = router;