const User =require("../models/Users");

const Product=require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);


	let newUser = new User({

				firstName: req.body.firstName,
				lastName:req.body.lastName,
				email: req.body.email,
				password:hashedPw,
				mobileNo:req.body.mobileNo

	});


		newUser.save()
		.then(result=> res.send(result))
		.catch(error => res.send(error)) 

	
};
module.exports.userLogin=(req,res)=>{
	
	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message:"User not Registered!"})
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

		if(isPasswordCorrect){			
			return res.send({accessToken: auth.createAccessToken(foundUser)});
		}else
		return res.send({message:"Incorrect Password!"})
		}

	})
}
module.exports.getUserDetails = (req,res)=>{
 
	User.findById(req.user.id) 
		.then(result => res.send(result))
		.catch(error => res.send(error))
};
module.exports.setAsAdmin = (req,res)=>{

	let update={
		isAdmin:true
	}
	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};
