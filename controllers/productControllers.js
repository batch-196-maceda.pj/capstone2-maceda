const Product = require("../models/Product");

module.exports.addProduct =(req,res)=>{
	
	let newProduct = new Product({

				name: req.body.name,
				description:req.body.description,
				price: req.body.price

	})


		newProduct.save()
		.then(result=> res.send(result))
		.catch(error => res.send(error))
};
module.exports.activeProducts = (req,res) =>{
	
	Product.find({isActive:true})
	.then(result=> res.send(result))
	.catch(error => res.send(error))

}
module.exports.singleProduct = (req,res)=>{


	//console.log(req.params)
//console.log(req.params.courseId)

	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

	
}
module.exports.updateProduct =(req,res)=>{

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))	
	// console.log(req.body);

}
module.exports.archiveProducts = (req,res)=>{

	let update={
		isActive:false
	}
	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}